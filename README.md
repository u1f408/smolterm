# smolterm

A no_std-compatible terminal emulator library, designed for [polymorphOS][].

[polymorphOS]: https://git.kat.net.nz/alxce/polymorphOS
